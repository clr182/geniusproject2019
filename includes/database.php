<?php

  require_once('utils/include-guard.php');
  require_once('connect.php');

  # Create a re-usable PDO object
  # Should we check for a certain type of error here?
  $app_db_connection = new PDO(
    'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_DB_NAME . ';charset=utf8mb4',
    Config::DB_USER,
    Config::DB_PASS,
    Config::PDO_CONFIG
  );

?>