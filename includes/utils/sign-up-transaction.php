<?php 

$name= $_POST["name"];
$age = $_POST["age"];


$name = ltrim(rtrim(filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING)));
if (empty($name))
{
    header("location: ../view/home.php?error=1");
    exit();
}
$age = ltrim(rtrim(filter_input(INPUT_POST, "age", FILTER_SANITIZE_INT)));
if (empty($age))
{
    header("location: ../view/home.php?error=1");
    exit();
}





require_once "Connect.php";


$dbConnection = new PDO("mysql:host=$db_host;dbname=$db_database", $db_user, $db_password);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   // set the PDO error mode to exception


/* Perform Query */
$query = "INSERT INTO users (name, age) VALUES(:name, :age)";
$statement = $dbConnection->prepare($query);
$statement->bindParam(":name", $name, PDO::PARAM_STR);
$statement->bindParam(":age", $age, PDO::PARAM_STR);
$statement->execute();



/* Provide feedback that the record has been added */
if ($statement->rowCount() > 0)
{
    echo "<p>Record successfully added to database.</p>";
}
else
{
    echo "<p>Record not added to database.</p>";
}

?>
